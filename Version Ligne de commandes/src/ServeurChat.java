import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class ServeurChat implements ServeurChatItf {

	/**
	 * Gestion de la lsite des entités utilisateurs.
	 */
	static ArrayList<entiteClient> listeEntiteClient = new ArrayList<entiteClient>();

	/**
	 * Gestion des écrivains / lecteurs de logs
	 */
	static BufferedWriter logWriter = null;
	static BufferedReader logReader = null;

	public ServeurChat() {
		// On prépare le fichier de LOG
		try {
			logWriter = Files.newBufferedWriter(Paths.get("log.txt"), Charset.forName("UTF-8"));
			logReader = Files.newBufferedReader(Paths.get("log.txt"), Charset.forName("UTF-8"));
		} catch (IOException e) {
			System.err.println("Error in ServeurChat Constructeur :" + e);
		}
	}

	public boolean joinChat(CClientItf clientItf, String pseudo) {
		boolean answer = false;

		try {
			// On créé le client
			entiteClient entiteTMP = new entiteClient(pseudo, clientItf);
			
			// On lui envoit l'historique
			casterHistorique(clientItf);
			
			if(listeEntiteClient.size()!=0){
				//On lui donne la liste des connecté
				clientItf.recevoirMessage("SERVEUR > La liste des connecté est : " + getListeConnecte());
			} else {
				clientItf.recevoirMessage("SERVEUR > Il n'y a aucun connecté");
			}
		
			
			// On l'ajoute à la liste
			listeEntiteClient.add(entiteTMP);

			// Un nouveau est là
			System.out.println("Nouveau Client" + pseudo);


			

			answer = true;
		} catch (Exception e) {
			System.err.println("Error in ServeurChat Join :" + e);
		}

		return answer;
	}

	public boolean sendMessage(CClientItf clientItf, String message) {
		boolean answer = true;

		// On récupère l'entité client correspondante
		entiteClient entiteTMP = getClient(clientItf);
		String pseudo = "inconnu";

		// Si il est bien dans la liste, en l'enlève.
		if (entiteTMP != null) {
			pseudo = entiteTMP.pseudo;
		}

		//On envoit le message à tout le monde.
		answer = sendMessage(pseudo,message);
		
		return answer;
	}
	
	

	public boolean quitChat(CClientItf clientItf) {
		
		
		// On récupère l'entité client correspondante
		entiteClient entiteTMP = getClient(clientItf);
		String pseudo = "inconnu";

		// Si il est bien dans la liste, en l'enlève.
		if (entiteTMP != null) {
			pseudo = entiteTMP.pseudo;
		}

		sendMessage("SERVEUR",pseudo + " s'est déconnecté.");
		
		// Si il est bien dans la liste, en l'enlève.
		if (entiteTMP != null) {

			// On supprime l'user de la base.
			listeEntiteClient.remove(entiteTMP);
			return true;
		}

		return false;
	}

	public entiteClient getClient(CClientItf clientItf) {

		// On parcours la liste
		for (int i = 0; i < listeEntiteClient.size(); i++) {
			// Si l'un de ses attributs correspondent
			if (listeEntiteClient.get(i).skeletonClient.equals(clientItf)) {
				// On le retourne
				return listeEntiteClient.get(i);
			}
		}

		return null;
	}

	/**
	 * Permet de récupérer l'historique des conversations
	 * 
	 * @param entiteCible
	 *            : la cible qui doit recevoir l'historique de conversation.
	 */
	public static void casterHistorique(CClientItf clientItf) {
		// NOTE : on pourrait limiter le nombre de lignes renvoyées .. mais ce
		// n'est pas l'objet du TP. (Vision grande échelle)

		try {

			// Some greetings
			clientItf.recevoirMessage("== Récupération de l'historique de conversation ==");

			// On récupère tout le contenu
			String line = "";
			while ((line = logReader.readLine()) != null) {
				clientItf.recevoirMessage(line);
			}

			// On salut
			clientItf.recevoirMessage("== Fin de la récupération de l'historique de conversation ==");
		} catch (Exception e) {
			System.err.println("Error in ServeurChat cast Historique :" + e);
		}
	}
	
	private boolean sendMessage(String pseudo, String message) {
		boolean answer = true;

		// On constitue la chaîne à Broadcast/mettre en historique/ etc.
		String txtDate = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.FRANCE).format(new Date());
		String tmpToBroadcast = txtDate + "| " + pseudo + "> " + message;

		// On écrit dans les logs
		try {
			logWriter.append(tmpToBroadcast); // On ajoute la ligne aux logs
			logWriter.newLine(); // Aller à la ligne suivante
			logWriter.flush(); // libère le buffer
		} catch (Exception e) {
			System.err.println("Error in ServeurChat :" + e);
		}

		// On broadcast le message à tout le monde en spécifiant le pseudo
		for (int i = 0; i < listeEntiteClient.size(); i++) {
			try {
				CClientItf htmp = (CClientItf) listeEntiteClient.get(i).skeletonClient;
				htmp.recevoirMessage(tmpToBroadcast);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				System.err.println("Error in ServeurChat - sendMessage Join N° " + i + " :" + e);
				answer = false;
			}
		}
		return answer;
	}
	
	private String getListeConnecte() {
		String liste = "";
		
		for (int i = 0; i < listeEntiteClient.size(); i++) {
			liste += listeEntiteClient.get(i).pseudo;
			if(i != listeEntiteClient.size()-1){
				liste += ", ";
			}
		}
		
		return liste;
	}
	
	
}
