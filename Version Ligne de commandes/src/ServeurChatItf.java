import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ServeurChatItf extends Remote {

    /** Methode remote permettant de rejoindre le chat d'un serveur de chat.
     * @param clientItf : L'interface du client, permettant de l'identifier et de l'enregistrer sur le serveur
     * @param pseudo : String représentant le pseudo du client. Vérifications coté programme client (non nul ...)
     * @return : un boolean représentant si l'ajout s'est bien déroulé.
     * @throws RemoteException : Si problème avec cet objet remote
     */
    boolean joinChat(CClientItf clientItf, String pseudo) throws RemoteException;
    
    /** Permet au client d'envoyer un message au serveur. Le message sera broadcasté.
     * @param clientItf : L'identifiant du client qui envoi le message
     * @param message : le contenu du message envoyé
     * @return : un boolean spécifiant si le message a été correctement broadcast/reçu
     * @throws RemoteException : Si problème avec cet objet remote 
     */
    boolean sendMessage(CClientItf clientItf, String message) throws RemoteException;
    
    /**
     * @param clientItf : L'identifiant du client qui envoi le message 
     * @return : un boolean spécifiant si le client a pu quitter le chat.
     * @throws RemoteException : Si problème avec cet objet remote
     */
    boolean quitChat(CClientItf clientItf) throws RemoteException;

}
