import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class ClientLauncher {
	 public static void main(String[] args) {
	        String host = (args.length < 2) ? null : args[0];
	        String pseudo = (args.length < 2) ? null : args[1];
	        
	        try {
	            Registry registry = LocateRegistry.getRegistry(host);
	            ServeurChatItf stub = (ServeurChatItf) registry.lookup("ServerChat1");
	            
	            CClient client = new CClient();
	            CClientItf clientItf = (CClientItf) UnicastRemoteObject.exportObject(client, 0);
	            
	            stub.joinChat(clientItf,args[1]);
	            
	            boolean running = true;
	            BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
	            String line;
	            while(running){
	            	line = stdIn.readLine();
	            	if (line.equals("quit")){
	    				stub.quitChat(clientItf);
	    				running = false;
	    				break;
	    			} else {
	    				//On envoit le message
		            	stub.sendMessage(clientItf,line);
	    			}
	    			stdIn = new BufferedReader(new InputStreamReader(System.in));
	            }
	        } catch (Exception e) {
	            System.err.println("Client exception: " + e.toString());
	            e.printStackTrace();
	        }
	    }
}
