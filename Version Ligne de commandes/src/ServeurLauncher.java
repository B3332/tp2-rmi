import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class ServeurLauncher {

	static int PORT = 1099;

	public static void main(String args[]) {

		try {

			// Note : on le récupérerait directement si on était full local.
			// Registry registry =
			// Lancement du rmi Registry
			LocateRegistry.createRegistry(PORT);

			System.err.println("Création de l'objet ServeurChat.");
			ServeurChat obj = new ServeurChat();

			System.err.println("Création de stub.");
			ServeurChatItf stub = (ServeurChatItf) UnicastRemoteObject.exportObject(obj, 0);

			System.err.println("Récupération du registre");
			// Bind the remote object's stub in the registry
			Registry registry = LocateRegistry.getRegistry("localhost", PORT);
			
			System.err.println("Enregistrement de l'objet dans le registre");
			registry.bind("ServerChat1", stub);

			System.err.println("Server ready");

		} catch (Exception e) {
			System.err.println("Server exception: " + e.toString());
			e.printStackTrace();
		}
	}
}