import java.io.BufferedReader;
import java.io.PrintStream;

public class entiteClient {
	// Objets directement construits
	public String pseudo = "";
	public CClientItf skeletonClient = null;

	// Constructeur
	entiteClient(String pseudo, CClientItf interfaceClient) {
		this.pseudo = pseudo;
		this.skeletonClient = interfaceClient;
	}
}