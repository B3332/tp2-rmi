import java.rmi.Remote;
import java.rmi.RemoteException;

public interface CClientItf extends Remote {
	void recevoirMessage(String message) throws RemoteException;
}
